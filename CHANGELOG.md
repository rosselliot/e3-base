# CHANGELOG
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [7.0.5]
Update to EPICS base 7.0.5

### New Features
* See documentation for EPICS base: https://github.com/epics-base/epics-base/blob/7.0/documentation/RELEASE_NOTES.md#epics-release-705

### Bugfixes
* Patch softIocPVA to avoid double call to iocInit()

### Other changes
* Add -std=c++11 to linux-x86_64 builds

## [7.0.4]
Previously installed version.
